﻿using UnityEngine;
using System.Collections;

public class SceneFading : MonoBehaviour {
	
	private double 		alpha;
	public 	Texture 	tex;

	private IEnumerator AlphaTimer(float time)
	{
		var rate = 1.0/time;
	 	var way = alpha > 0 ? 1 : 0;
		
		if (way == 1) {
			for (alpha = 1.0f; alpha > 0.0f; alpha -= Time.deltaTime * rate)
			{
				yield return true;
			}
			alpha = 0.0f;
		} else {
			for (alpha = 0.0f; alpha < 1.0f; alpha += Time.deltaTime * rate)
			{
				yield return true;
			}
			alpha = 1.0f;
		}
	}
	
	public void FadeOut(float time)
	{
		alpha = 1.0f;
		StartCoroutine(AlphaTimer(time));
	}

	public void FadeIn(float time)
	{
		alpha = 0.0f;
		StartCoroutine(AlphaTimer(time));
	}

	void OnGUI () {
		Color thisColor = GUI.color;
		thisColor.a = (float)alpha;

		GUI.color = thisColor;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), tex);
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
