﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Activationable : MonoBehaviour {
	
	public List<Animation> anims;
	public bool activated;
	public Transform[] infoLights;
	public Transform[] targetElements;
	public bool animationStart = false;
	
	public float infoLightsActivationDelay; // Ammout of time for each "Toggle" light to be activated and deactivated. 0 for instant
	public float infoLightsDeactivationDelay; // Ammout of time for each "Toggle" light to be activated and deactivated. 0 for instant
	public Transform activatorSurface;
	
	public Material onlineMaterial;
	public Material offlineMaterial;
	
	private List<Transform> collidersTrs;
	
	// Use this for initialization
	public void Start () {
		
		collidersTrs = new List<Transform>();
		activated = false;
		activatorSurface.gameObject.animation.wrapMode = WrapMode.Once;
		activatorSurface.gameObject.animation.Stop();
		
	}

	// Update is called once per frame
	public void Update () {
	
	}
	
	private IEnumerator ToggleLight(GameObject elem, bool state)
	{
		Transform element = elem.transform.FindChild("Info Locker Content");
		Transform lightElement = elem.transform.FindChild("Point light");
		
		if (state) {
			yield return new WaitForSeconds(infoLightsActivationDelay);
			element.GetComponent<MeshRenderer>().material = onlineMaterial;
			lightElement.GetComponent<Light>().enabled = true;
			lightElement.GetComponent<Light>().color = new Color(0.0f, 1.0f, 0.0f, 1.0f);
		} else {
			yield return new WaitForSeconds(infoLightsDeactivationDelay);
			element.GetComponent<MeshRenderer>().material = offlineMaterial;			
			lightElement.GetComponent<Light>().enabled = false;
		}
		elem.GetComponent<AudioSource>().Play();
		
		yield return true;
	}
	
	private IEnumerator AnimateLights(bool state)
	{
		if (audio)
			audio.Play();
		foreach (Transform light in infoLights)
		{
			yield return StartCoroutine(ToggleLight(light.gameObject, state));
		}
		yield return true;
	}
	
	private IEnumerator ExecuteAnimations()
	{
		yield return StartCoroutine("AnimateLights", activated);
		if (activated == true)
		{
			animationStart = true;
			foreach (Animation anim in anims) 
			foreach (AnimationState animstate in anim.animation) 
	    	{
				animstate.speed = 1;
				if (animstate.time < 0)
					animstate.time = 0;
			}
			foreach (Transform elem in targetElements) {
				elem.SendMessage("ActivatorTriggerActive", this.gameObject);	
			}
		} else {
			animationStart = false;
			foreach (Animation anim in anims)
			foreach (AnimationState animstate in anim.animation) 
	    	{
				animstate.speed = -1;
				if (animstate.time > animstate.length)
					animstate.time = animstate.length;
			}
			foreach (Transform elem in targetElements) {
				elem.SendMessage("ActivatorTriggerInactive", this.gameObject);	
			}
		}
		foreach (Animation anim in anims)
			anim.Play();
		yield return true;
	}
	
	public void CollisionAction(Transform collisionInfo, bool action)
	{
		collidersTrs.Remove(collisionInfo.transform);
		if (action == true)
			collidersTrs.Add(collisionInfo.transform);
		
		if (collidersTrs.Count == 0) {
			activated = false;
		} else {
			activated = true;
		}
		
		if (activated) {
	        //print("Enter in contact with " + collisionInfo.transform.name);
			if (activatorSurface.gameObject.animation["Activate"])
			{
				activatorSurface.gameObject.animation["Activate"].speed = 1;
				activatorSurface.gameObject.animation["Activate"].time = 0;
				activatorSurface.gameObject.animation.Play("Activate");
			}
		} else {
			activated = false;
	        //print("No longer in contact with " + collisionInfo.transform.name);
			if (activatorSurface.gameObject.animation["Activate"])
			{
				activatorSurface.gameObject.animation["Activate"].speed = -1;
				activatorSurface.gameObject.animation["Activate"].time = activatorSurface.gameObject.animation["Activate"].length;
				activatorSurface.gameObject.animation.Play("Activate");
			}
		}
		StopAllCoroutines();
		StartCoroutine("ExecuteAnimations");
	}
	
	public void OnColliderDeletion(Transform elem)
	{
		CollisionAction(elem, false);
	}
	
	public void OnTriggerEnter(Collider collisionInfo) {

		/* Disable activation */
		if (infoLightsDeactivationDelay == -1 || 
		    (collisionInfo.collider.tag != "Player" && collisionInfo.collider.tag != "Ghost" && collisionInfo.collider.tag != "Destroying" && collisionInfo.collider.tag != "Movable"))
			return ;
		if (collisionInfo.collider.tag != "Movable")
			collisionInfo.GetComponent<ControllableSphere>().SetTriggerColision(this.transform);
		CollisionAction(collisionInfo.gameObject.transform, true);
	}
	
    public void OnTriggerExit(Collider collisionInfo) {
		
		/* Disable deactivation */
		if (infoLightsDeactivationDelay == -1 || 
		    (collisionInfo.collider.tag != "Player" && collisionInfo.collider.tag != "Ghost" && collisionInfo.collider.tag != "Destroying" && collisionInfo.collider.tag != "Movable"))
			return ;
		if (collisionInfo.collider.tag != "Movable")
			collisionInfo.GetComponent<ControllableSphere>().SetTriggerColision(null);
		CollisionAction(collisionInfo.gameObject.transform, false);
	}
}
