﻿using UnityEngine;
using System.Collections;

public class ChooseLevel : MonoBehaviour {

	private int level = 1;
	public int maxLevel = 13;
	public Material activeMaterial;
	public Material inactiveMaterial;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Paused.getPaused())
			return;
		Paused.setMenuing(true);
		if (Input.GetButtonDown("Fire1"))
		{
			GameObject.Find("Shared Elements").GetComponent<PauseMenu>().SetUnpaused();
			GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToMainMenuCR();
		}

		foreach (Transform child in transform)
		{
			if (child.name != "Level Button")
				continue;
			TextMesh txt = child.Find("Text").GetComponent<TextMesh>();
			Transform ent = child.Find("Content").GetChild(0);
			if (txt.text != level.ToString())
				ent.GetComponent<MeshRenderer>().material = inactiveMaterial;
			else
				ent.GetComponent<MeshRenderer>().material = activeMaterial;
		}

		if (Input.GetButtonDown("Horizontal"))
		{
			float value = Input.GetAxis("Horizontal");
			if (value < 0.0f)
				--level;
			else
				++level;
			if (level > maxLevel)
				level = maxLevel;
			else if (level < 1)
				level = 1;
		}
		if (Input.GetButtonDown("Jump"))
		{
			Paused.setMenuing(false);
			GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().currentLevelId = level - 1;	
			GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToGameCR();
		}

	}
}