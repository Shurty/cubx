﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnerScript : MonoBehaviour {
	
	/* Properties */
	public GameObject player;
	public GameObject spawnPosition;
	public bool initialSpawn = true;
	public int maxPop = 1;
	public bool useOnce = false;
	public Material availableMaterial;
	public Material emptyMaterial;

	private List<GameObject> listSpawns = new List<GameObject>();
	
	void Start () {
		if (initialSpawn)
			SpawnBall();
		UpdateDisplay();
	}
	
	bool TouchEvent()
	{
		int wd = Screen.width;
		
		foreach (Touch touch in Input.touches) {
			bool inpos = touch.position.x > wd * 0.3 && touch.position.x < wd * 0.7;
			if (touch.phase == TouchPhase.Ended && inpos) {
				return (true);
			}
		}
		return (false);
	}
	
	void LateUpdate () 
	{
		if (Time.timeScale == 0)
			return;
		bool value = Input.GetKeyDown(KeyCode.Space);
		if (value || TouchEvent() || Input.GetKeyDown(KeyCode.JoystickButton0))
		{
			SpawnBall();
		}
	}
	
	public void Activate() 
	{
		GameObject[] olds =  GameObject.FindGameObjectsWithTag("Spawner");
		
		foreach (GameObject obj in olds)
		{
			obj.tag = "GhostSpawner";
			//obj.GetComponent<SpawnerScript>().reset();
		}
		tag = "Spawner";
	}
	
	public void Reset()
	{
		foreach (GameObject obj in listSpawns)
		{
			StartCoroutine(obj.GetComponent<ControllableSphere>().Remove());
		}
		listSpawns.Clear();
	}
	
	public GameObject SpawnBall() 
	{
		audio.Play();
		if (listSpawns.Count >= maxPop)
		{
			if (useOnce)
				return (null);
			Reset();
		}
		if (player)
		{
			GameObject g = Instantiate(player,spawnPosition.transform.position, player.transform.rotation) as GameObject;
			
			g.GetComponent<PlayerControll>().DeactiveOthers();
			// Coroutine creating ball launch animation
			StartCoroutine(g.GetComponent<ControllableSphere>().InitializeSphere(this.gameObject));
			
			listSpawns.Add(g);
			// Update the displayed number and light effect
			UpdateDisplay();
			
			return (g);
		}
		return (null);
	}
	
	private void UpdateDisplay()
	{
		Transform displayController = this.transform.FindChild("Info Display Base");
		Transform displayLocker = displayController.transform.FindChild("Info Locker Content");
		Transform pointLight = displayController.transform.FindChild("Point light");
		int remainingCount = maxPop - listSpawns.Count;
		
		displayLocker.transform.FindChild("Text Element").GetComponent<TextMesh>().text = remainingCount.ToString();
		if (remainingCount == 0) {
			displayLocker.GetComponent<MeshRenderer>().materials[0] = emptyMaterial;
			pointLight.GetComponent<Light>().color = new Color(0.95f, 0.1f, 0.1f, 1.0f);
		} else {
			displayLocker.GetComponent<MeshRenderer>().materials[0] = availableMaterial;
			pointLight.GetComponent<Light>().color = new Color(0.95f, 0.95f, 0.1f, 1.0f);
		}
	}
}
