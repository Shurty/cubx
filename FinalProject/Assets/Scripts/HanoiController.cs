﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HanoiController : MonoBehaviour {

	public Activationable oneToRightInterruptor;
	public Activationable twoToLeftInterruptor;
	public Activationable twoToRightInterruptor;
	public Activationable threeToLeftInterruptor;

	private List< List<GravityAreaScript> > pillars = new List< List<GravityAreaScript> >(3);

	public GravityAreaScript[] pillarOne;
	public GravityAreaScript[] pillarTwo;
	public GravityAreaScript[] pillarThree;

	int lastActivated = 0;

	void addPillar(GravityAreaScript[] pillar)
	{
		pillars.Add(new List<GravityAreaScript> (pillar.Length));
		foreach (GravityAreaScript gravity in pillar) {
			pillars[pillars.Count - 1].Add(gravity);
		}
	}
	// Use this for initialization
	void Start () {
		addPillar (pillarOne);
		addPillar (pillarTwo);
		addPillar (pillarThree);
	}

	enum Direction {
		eLeft = -1,
		eRight = 1
	}

	void moveTo(Direction direction, GravityAreaScript from, int pillarNumber, int idxFrom, GameObject interruptor)
	{
		int idxTo = 0;
		for (int idxPillar = pillarNumber + (int)direction;
		     idxPillar < pillars.Count && idxPillar >= 0;
		     idxPillar += (int)direction)
		{
			foreach (GravityAreaScript to in pillars[idxPillar])
			{
				if (to.isActive && idxTo < idxFrom)
					break;
				++idxTo;
			}
			if (idxTo == pillars[idxPillar].Count)
			{
				from.transform.SendMessage("TriggerDesactivate", interruptor);
				pillars[idxPillar][idxFrom].transform.SendMessage("TriggerActivate", interruptor);
				break;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		int i = 0;
		if ((oneToRightInterruptor.animationStart == false && lastActivated == 1) ||
		    (twoToLeftInterruptor.animationStart == false && lastActivated == 2) ||
		    (twoToRightInterruptor.animationStart == false && lastActivated == 3) ||
		    (threeToLeftInterruptor.animationStart == false && lastActivated == 4))
			lastActivated = 0;
		if (oneToRightInterruptor.animationStart && lastActivated != 1) {
			foreach (GravityAreaScript gravity in pillarOne)
			{
				if (gravity.isActive == true)
				{
					lastActivated = 1;
					moveTo(Direction.eRight, gravity, 0, i, oneToRightInterruptor.gameObject);
					break;
				}
				++i;
			}
		}
		else if (twoToLeftInterruptor.animationStart && lastActivated != 2) {
			foreach (GravityAreaScript gravity in pillarTwo)
			{
				if (gravity.isActive == true)
				{
					lastActivated = 2;
					moveTo(Direction.eLeft, gravity, 1, i, twoToLeftInterruptor.gameObject);
					break;
				}
				++i;
			}
		}
		else if (twoToRightInterruptor.animationStart && lastActivated != 3) {
			foreach (GravityAreaScript gravity in pillarTwo)
			{
				if (gravity.isActive == true)
				{
					lastActivated = 3;
					moveTo(Direction.eRight, gravity, 1, i, twoToRightInterruptor.gameObject);
					break;
				}
				++i;
			}
		}
		else if (threeToLeftInterruptor.animationStart && lastActivated != 4) {
			foreach (GravityAreaScript gravity in pillarThree)
			{
				if (gravity.isActive == true)
				{
					lastActivated = 4;
					moveTo(Direction.eLeft, gravity, 2, i, threeToLeftInterruptor.gameObject);
					break;
				}
				++i;
			}
		}
	}
}
