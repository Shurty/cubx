﻿using UnityEngine;
using System.Collections;

public class GUIButton : MonoBehaviour {
	
	public Material activeMaterial;
	public Material inactiveMaterial;	

	// Switching to a level (if golevel is page action)
	public int levelSwitch = 0;
	
	public enum PageAction {
		RESUME,
		MAINMENU,
		EXIT,
		SELECTLEVEL,
		GOLEVEL,
        SWITCH_ACCELEROMETER,
        CREDITS_LEVEL
	}
	
	public PageAction buttonAction;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//TouchEvent();
	}
	
	public void SetHovered(bool state)
	{
		int blocksCount = transform.parent.transform.Find("Content").childCount;	
		while (blocksCount > 0)
		{
			Transform ent = transform.parent.transform.Find("Content").GetChild(blocksCount - 1);
			if (state == false)
				ent.GetComponent<MeshRenderer>().material = inactiveMaterial;
			else
				ent.GetComponent<MeshRenderer>().material = activeMaterial;
			blocksCount--;	
		}		
	}
	
	/*bool TouchEvent()
	{
		int wd = Screen.width;
		RaycastHit theHit;
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Ended) {
				if (collider.Raycast(GameObject.Find("Main Camera").GetComponent<Camera>().ScreenPointToRay(touch.position), out theHit, 83))
				{
					OnMouseDown();
		        }
			}
		}
		return (false);
	}
	*/
	void OnMouseEnter() {
		SetHovered(true);
	}
	
	void OnMouseDown() {
		Paused.setMenuing(false);
		Paused.setPaused(true);
		if (buttonAction == PageAction.EXIT) {
			Application.Quit();	
		} else if (buttonAction == PageAction.RESUME) {
			GameObject.Find("Shared Elements").GetComponent<PauseMenu>().SetUnpaused();
		} else if (buttonAction == PageAction.MAINMENU) {
			GameObject.Find("Shared Elements").GetComponent<PauseMenu>().SetUnpaused();
			GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToMainMenuCR();	
		} else if (buttonAction == PageAction.SELECTLEVEL) {
			GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToLevelSelect();
		} else if (buttonAction == PageAction.GOLEVEL) {
			GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().currentLevelId = levelSwitch - 1;	
			GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToGameCR();	
		} else if (buttonAction == PageAction.SWITCH_ACCELEROMETER) {
            if (PlayerControll.switchAccelerometer())
				transform.parent.transform.Find("Content").GetChild(0).GetComponent<MeshRenderer>().material = activeMaterial;
			else
				transform.parent.transform.Find("Content").GetChild(0).GetComponent<MeshRenderer>().material = inactiveMaterial;
		} else if (buttonAction == PageAction.CREDITS_LEVEL) {
            GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToCreditsCR();
        }
	}
	
	void OnMouseExit() {
		SetHovered(false);
	}
}
