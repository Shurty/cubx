﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {
	
	public Transform pauseGUI;

	private bool isActive = true;
	private bool paused = false;

	private int level = 1;
	private int maxLevel = 3;
	public Material activeMaterial;
	public Material inactiveMaterial;

	// Use this for initialization
	void Start () {
		SetUnpaused();
		Paused.setPaused(true);
	}
	
	// Update is called once per frame
//	void Update () {
//	
//	} Late
	
	void Update() {
		if (!Paused.getPaused())
			return;
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (paused) {
				SetUnpaused();
			} else {
				SetPaused();
			}
		}


		int a = this.gameObject.transform.GetChild(1).childCount;
		for(int i = 0; i < a; i++)
		{
			GameObject child = this.gameObject.transform.GetChild(1).GetChild(i).gameObject;
			if(child.gameObject.activeSelf && child.name != "Pause Menu GUI")
				return;
		}

		if (Input.GetButton("Jump"))
		{
			if (level == 3) {
				Application.Quit();	
			} else if (level == 2) {
				Paused.setMenuing(true);
				GameObject.Find("Shared Elements").GetComponent<PauseMenu>().SetUnpaused();
				GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToMainMenuCR();	
			} else if (level == 1) { //Resume
				GameObject.Find("Shared Elements").GetComponent<PauseMenu>().SetUnpaused();
			}
		}

		if (Input.GetButtonDown("Vertical"))
		{
			float value = Input.GetAxis("Vertical");
			if (value < 0.0f)
				++level;
			else
				--level;
			if (level > maxLevel)
				level = maxLevel;
			else if (level < 1)
				level = 1;
		}
//		inutile pour smartphone, a garder pour la version TV
//		int count = 1;
//		foreach (Transform child in this.gameObject.transform.GetChild(1).GetChild(0).GetChild(0).transform)
//		{
//			if (count != level)
//				child.Find("Content").GetChild(0).GetComponent<MeshRenderer>().material = inactiveMaterial;
//			else
//				child.Find("Content").GetChild(0).GetComponent<MeshRenderer>().material = activeMaterial;
//			++count;
//		}
	}
	
	public void SetActive(bool res) {
		isActive = res;
	}

	public bool getActive() {
		return isActive;
	}

	public void SetPaused() {
		pauseGUI.gameObject.SetActive(true);
		Paused.setPaused(false);
		AudioListener.pause = true;
		paused = true;
		transform.FindChild("HUD Controller").GetComponent<GUIController>().ResetButtonState(pauseGUI);
	}
	
	public void SetUnpaused() {
		pauseGUI.gameObject.SetActive(false);		
		AudioListener.pause = false;
		paused = false;
		Paused.setPaused(true);
	}

}
