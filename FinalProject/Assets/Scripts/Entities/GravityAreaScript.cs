﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GravityAreaScript : MonoBehaviour {
	
	public bool isActive;
	private List<GameObject> insideObjects;
	public bool originalState;
	public float value = 1.0f;
	public bool legacy = false;
	
	// Use this for initialization
	void Start () {
		insideObjects = new List<GameObject>();
		isActive = originalState;
		Initialize();
	}
	
	void Initialize()
	{
		if (!isActive) {
			if (transform.FindChild("Particle System"))
				transform.FindChild("Particle System").GetComponent<ParticleSystem>().Stop();
			foreach (GameObject obj in insideObjects)
			{
				if (!obj) {
					continue ;
				} else {
					ResetGravityOnObject(obj);
				}
			}
		} else {
			if (transform.FindChild("Particle System"))
				transform.FindChild("Particle System").GetComponent<ParticleSystem>().Play();
			foreach (GameObject obj in insideObjects)
			{
				if (!obj) {
					continue ;
				} else {
					SetGravityOnObject(obj);
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void TriggerActivate(GameObject activator)
	{
		isActive = true;
		Initialize();
	}

	void TriggerDesactivate(GameObject activator)
	{
		isActive = false;
		Initialize();
	}

	void ActivatorTriggerActive(GameObject activator)
	{
		isActive = !originalState;	
		Initialize();
	}

	void ActivatorTriggerInactive(GameObject activator)
	{
		isActive = originalState;	
		Initialize();
	}

	void OnTriggerStay(Collider ent)
	{
		if (legacy || !ent.GetComponent<Rigidbody>())
			return ;
		insideObjects.Add(ent.gameObject.transform.gameObject);
		if (!isActive)
			return ;
		SetGravityOnObject(ent.gameObject);
		
	}

	void OnTriggerEnter(Collider ent)
	{
		if (!legacy || !ent.GetComponent<Rigidbody>())
			return ;
		insideObjects.Add(ent.gameObject.transform.gameObject);
		if (!isActive)
			return ;
		SetGravityOnObject(ent.gameObject);
		
	}

	private void SetGravityOnObject(GameObject ent)
	{
		if (!ent)
			return ;
		ent.GetComponent<Rigidbody>().useGravity = false;
	    if (!legacy) {
			if (ent.gameObject.GetComponent<ConstantForce>())
				ent.gameObject.GetComponent<ConstantForce>().force = (-Physics.gravity.y * transform.forward * value);
			else
				ent.gameObject.AddComponent<ConstantForce>().force = (-Physics.gravity.y * transform.forward * value);
		}
		else if (!ent.gameObject.GetComponent<ConstantForce>()) {
			ent.gameObject.AddComponent<ConstantForce>().force = (-Physics.gravity);
		}
	}
	
	private void ResetGravityOnObject(GameObject ent)
	{
		if (!ent)
			return ;
		ent.GetComponent<Rigidbody>().useGravity = true;
		Component.Destroy(ent.GetComponent<ConstantForce>());
		if (ent.tag == "Player") {
			GameObject camera = GameObject.Find("Main Camera");
			float targetAng = camera.GetComponent<FollowCamera>().rotationAngle.z;
			if (targetAng >= 359)
				targetAng = 0;
			else
				targetAng = 359;

			//camera.GetComponent<FollowCamera>().rotationAngle = new Vector3(15, 0, targetAng);
		}
	}
	
	void OnTriggerExit(Collider ent)
	{
		if (!ent.GetComponent<Rigidbody>())
			return ;
		insideObjects.Remove(ent.gameObject.transform.gameObject);
		if (!isActive)
			return ;
		ResetGravityOnObject(ent.gameObject);
	}

}
