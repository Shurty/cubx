CUBX
======

3D environement 2D platformer based on the Portal (Valve) theme. Complete a set of challenges and puzzles with increasing mechanics & difficulty.

Features
--------

* 11 levels

Platforms
--------

* PC (Keyboard)
* Android (Phone + Tablet)
* iOS (Phone + Tablet)

Usage
-------

Unity3D crossplatform

* Load the project with unity after cloning the repository

Development Quality
-------

Documentation: None - Feel free to make one  
Code quality: Low - Protoype, low experience on engine, quick development  

Dependancies & Engines
-------

* Unity3D

Licence
-------
This project is under MIT Licence.

The MIT License (MIT)

Copyright (c) 2015 Jeremy Dubuc

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.