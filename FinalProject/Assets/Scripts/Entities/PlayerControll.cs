﻿using UnityEngine;
using System.Collections;

public class PlayerControll : MonoBehaviour {
	public float maxForce = 15.0f;
	
	Vector3 force;
	
	public Material unActivMat;
	public Material activMat;
	
	// Use this for initialization
	void Start () {
		force = new Vector3(0, 0, 0);
	}
		
	float TouchEvent()
	{
		int wd = Screen.width;
		float val = 1.0f;
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Began) {
				if (touch.position.x > wd * 0.7)
					return (val);
				if (touch.position.x < wd * 0.3)
					return (val * -1);
			}
		}
		return (0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (Paused.getPaused())
			return;

		if (tag == "Player")
		{
            float value;
            if (!useAccelerometer)
                value = Input.GetAxis("Horizontal");
            else
            {
                value = Input.acceleration.x;

                if (value < 0.1 && value > -0.1)
                    value = 0;
                value *= 4;
                if (value > 1)
                    value = 1;
                else if (value < -1)
                    value = -1;
            }
            float currentForce = GetComponent<Rigidbody>().velocity.x;
			
			if (TouchEvent() != 0.0f)
				value = TouchEvent();
			force.x = maxForce * value * Time.deltaTime * 40;
			
			if ((currentForce > 0 && force.x < 0) || (currentForce < 0 && force.x > 0)) {
				force.x *= 4	;
			}
			
			GetComponent<Rigidbody>().AddForce(force);
		}
	}
	public void DeactiveOthers()
	{
		GameObject[] olds =  GameObject.FindGameObjectsWithTag("Player");
		
		foreach (GameObject obj in olds)
		{
			obj.tag = "Ghost";
			obj.renderer.material = unActivMat;
		}
	}
	
	public void activate() {
		DeactiveOthers();
		tag = "Player";
		this.renderer.material = activMat;
	}

    public static bool useAccelerometer = false;
    public static bool switchAccelerometer()
    {
        useAccelerometer = !useAccelerometer;
		return useAccelerometer;
    }
}
