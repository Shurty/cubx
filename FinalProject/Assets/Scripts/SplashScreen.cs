﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SplashScreen : MonoBehaviour {
	
	public List<GameObject> spawnedBodies;
	public List<Transform> spawners;
	public List<Transform> despawners;

	public float delay;
	public int chances;
	public int count;
	
	private float lastUpdate;
	
	// Use this for initialization
	void Start () {
		/*spawnedBodies = new ArrayList<GameObject>();
		spawners = new ArrayList<Transform>();
		despawners = new ArrayList<Transform>();*/
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeSinceLevelLoad > lastUpdate + delay)
		{
			lastUpdate = Time.timeSinceLevelLoad;
			for (int i = 0; i < count; i++)
			{
				int rnd = Random.Range(0, 100);
				if (rnd <= chances) {
					SpawnBody();	
				}
			}
		}
	}
	
	GameObject SelectRandomBody() {
		
		bool choosen = false;
		
		while (!choosen) {
			foreach (GameObject elem in spawnedBodies)
			{
				if (Random.Range(0, 100) >= 50) {					
					choosen = true;
					return (elem);
				}
			}
		}
		return (null);
	}
	
	void SpawnBody() {
		
		bool spawned = false;
		
		while (!spawned) {
			foreach (Transform spawner in spawners)
			{
				if (Random.Range(0, 100) >= 50) {
					
					spawned = true;
					GameObject g = Instantiate(SelectRandomBody(), 
						spawner.transform.position, 
						spawner.transform.rotation * Random.rotation) as GameObject;
					g.GetComponent<Rigidbody>().AddForce(Random.insideUnitSphere);
				}
			}
		}
		
	}
}
