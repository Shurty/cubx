﻿using UnityEngine;
using System.Collections;

public class Bridge : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator Example() {
		yield return new WaitForSeconds(0.5f);
		this.GetComponent<Rigidbody>().useGravity = true;
		this.GetComponent<Rigidbody>().isKinematic = false;
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Player")
		{
			StartCoroutine(Example());
		}
	}
}
