﻿using UnityEngine;
using System.Collections;

public class SharedEntity : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
		transform.FindChild("Scene Controller").GetComponent<SceneSwitch>().GoToSplash();
	}
	
	// Update is called once per frame
	void Update () {
		GameObject cam;
		
		if (GameObject.Find("Main Camera")) {
			cam = GameObject.Find("Main Camera");
			this.transform.position = cam.transform.position;
		}
	}
}
