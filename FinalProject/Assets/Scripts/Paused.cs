﻿using UnityEngine;
using System.Collections;

public static class Paused {
	public static bool paused;
	public static bool menuing;

	public static void setPaused(bool a)
	{
		paused = a;
	}
	
	public static bool getPaused()
	{
		return paused;
	}

	public static void setMenuing(bool a)
	{
		menuing = a;
	}
	
	public static bool getMenuing()
	{
		return menuing;
	}
}
