﻿using UnityEngine;
using System.Collections;

public class ExitScript : MonoBehaviour {
	
	private bool isActive;
	private bool originalState;
	public Material materialInactive;
	public Material materialActive;
	
	void ActivatorTriggerActive(GameObject activator)
	{
		isActive = !originalState;
		Initialize();
	}

	void ActivatorTriggerInactive(GameObject activator)
	{
		isActive = originalState;	
		Initialize();
	}
	
	private void Initialize()
	{
		if (!isActive) {
			SwapToGhost();
		} else {
			SwapToActive();	
		}
	}
	
	private IEnumerator SphereMoveEffect(GameObject sph)
	{
		Vector3 pos = sph.transform.position;

		sph.tag = "Untagged";
		sph.rigidbody.isKinematic = true;
		transform.FindChild("Particle System").GetComponent<ParticleSystem>().Play();
		for (float tm = 1.0f; tm > 0.0f; tm -= Time.deltaTime)
		{
			pos = sph.transform.position;
			sph.transform.position = Vector3.Lerp(pos, transform.position + new Vector3(0, 3, 0), 0.05f);
			yield return true;
		}
		StartCoroutine(sph.GetComponent<ControllableSphere>().FadeOut());
		sph.GetComponent<SphereCollider>().enabled = false;
		sph.rigidbody.isKinematic = false;
		GameObject.Find("HUD Controller").GetComponent<SceneFading>().FadeIn(1.0f);	
		yield return new WaitForSeconds(1);
		GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToNextLevel();
	}
	
	public void OnTriggerEnter(Collider col)
	{
		GameObject obj = col.gameObject;
		
		if (this.tag == "GhostExit")
			return ;
		audio.Play();
		
		if (obj.tag == "Player" || obj.tag == "Ghost") 
		{
			StartCoroutine(SphereMoveEffect(obj));
		}
	}

	public void OnTriggerStay(Collider col)
	{
		GameObject obj = col.gameObject;
		
		if (this.tag == "GhostExit")
			return ;
		audio.Play();
		
		if (obj.tag == "Player" || obj.tag == "Ghost") 
		{
			StartCoroutine(SphereMoveEffect(obj));
		}
	}


	// Use this for initialization
	public void Start () {
		isActive = true;
		if (tag == "GhostExit") {
			isActive = false;
		}
		originalState = isActive;
		Initialize();
	}
	
	public void SwapToGhost()
	{
		Transform infoDisplay = transform.FindChild("Info Display Exit");
		Transform infoLocker = infoDisplay.FindChild("Info Locker Content");
		
		tag = "GhostExit";
		transform.FindChild("Particle System Permanent").gameObject.SetActive(false);
		transform.FindChild("Particle System Offline").gameObject.SetActive(true);
		renderer.material.color = new Color(1.0f, 0.0f, 0.0f, 0.0f);
		infoLocker.renderer.material = materialInactive;
		infoDisplay.FindChild("Point light").GetComponent<Light>().color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
	}
	
	public void SwapToActive()
	{
		Transform infoDisplay = transform.FindChild("Info Display Exit");
		Transform infoLocker = infoDisplay.FindChild("Info Locker Content");
		
		tag = "Finish";
		transform.FindChild("Particle System Permanent").gameObject.SetActive(true);
		transform.FindChild("Particle System Offline").gameObject.SetActive(false);
		renderer.material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		
		transform.FindChild("Info Display Exit").FindChild("Info Locker Content").
			FindChild("Text Element").GetComponent<TextMesh>().text = "GO";	
		
		infoLocker.renderer.material = materialActive;
		infoDisplay.FindChild("Point light").GetComponent<Light>().color = new Color(0.0f, 1.0f, 0.0f, 1.0f);
	}
	
	// Update is called once per frame
	public void Update () {
		if (tag == "GhostExit") {
			if (Time.timeSinceLevelLoad % 2 > 1.8f) {
				transform.FindChild("Info Display Exit").FindChild("Info Locker Content").
					FindChild("Text Element").GetComponent<TextMesh>().text = "GO";				
			} else if (Time.timeSinceLevelLoad % 2 > 1.7f) {
				transform.FindChild("Info Display Exit").FindChild("Info Locker Content").
					FindChild("Text Element").GetComponent<TextMesh>().text = "C9";	
			} else {
				transform.FindChild("Info Display Exit").FindChild("Info Locker Content").
					FindChild("Text Element").GetComponent<TextMesh>().text = "-O";
			}
		}
	}
}
