﻿using UnityEngine;
using System.Collections;

public class SceneSwitch : MonoBehaviour {

	public int currentLevelId = 0;
	public bool bonusMode = false;

	private int currentBonusId = 0;

	// Use this for initialization
	void Start() {
	}
	
	void Initialize() {
		int levelIdDisplay = currentLevelId;
		
		GameObject.Find("HUD Controller").GetComponent<SceneFading>().FadeOut(1.0f);

		if (currentLevelId == 0) {
			return ;
		}
		if (GameObject.Find("Welcome Panel")) {
			Transform txPanel = GameObject.Find("Welcome Panel").transform;
			Transform textboard = txPanel.FindChild("Level Text");
			if (currentLevelId < 10) {
				textboard.GetComponent<TextMesh>().text = "0" + levelIdDisplay.ToString();
			} else {
				textboard.GetComponent<TextMesh>().text = levelIdDisplay.ToString();				
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void OnLevelWasLoaded()
	{
		Initialize();
	}
	
	public void GoToNextLevel() {
		// Pour l'instant pas de bonus mode, mais prochainement implemente
		if (bonusMode) {			
			if (Application.CanStreamedLevelBeLoaded("BonusLevel"+(currentBonusId+1))) {
				Application.LoadLevel("BonusLevel"+(currentBonusId+1).ToString());
				currentBonusId += 1;
			} else {
				bonusMode = false;
				GoToNextLevel();
			}
		} else {	
			if (Application.CanStreamedLevelBeLoaded("Level"+(currentLevelId+1))) {
				Application.LoadLevel("Level"+(currentLevelId+1).ToString());
			} else {
				Application.LoadLevel("Ending");
				currentLevelId += 1;
				return ;
			}
			currentLevelId += 1;
		}
	}
	
	public IEnumerator GoToGame() {
		
//		if (currentLevelId == 9) {
//			currentBonusId = 0;
//			bonusMode = true;
//		}
		
		GameObject.Find("HUD Controller").GetComponent<SceneFading>().FadeIn(1.0f);
		yield return new WaitForSeconds(1.0f);
		
		transform.parent.Find("HUD Controller").Find("Select Level Menu GUI").gameObject.SetActive(false);
//		transform.parent.Find("HUD Controller").Find("Main Menu GUI").gameObject.SetActive(false);
		transform.parent.GetComponent<PauseMenu>().SetActive(true);
        GoToNextLevel(); 
	}

    public IEnumerator GoToCredits()
    {
        GameObject.Find("HUD Controller").GetComponent<SceneFading>().FadeIn(1.0f);
        yield return new WaitForSeconds(1.0f);

//        transform.parent.Find("HUD Controller").Find("Main Menu GUI").gameObject.SetActive(false);
        transform.parent.GetComponent<PauseMenu>().SetActive(true);
        Application.LoadLevel("credits");
    }

	public void GoToLevelSelect() {		
		Transform tgtT = transform.parent.Find("HUD Controller").Find("Select Level Menu GUI");
		
		transform.parent.FindChild("HUD Controller").GetComponent<GUIController>().ResetButtonState(tgtT);
		transform.parent.Find("HUD Controller").Find("Pause Menu GUI").gameObject.SetActive(false);
		tgtT.gameObject.SetActive(true);
		transform.parent.GetComponent<PauseMenu>().SetActive(false);				
	}

    public void GoToGameCR()
    {
        StartCoroutine(GoToGame());
    }

    public void GoToCreditsCR()
    {
        StartCoroutine(GoToCredits());
    }
	
	public void GoToMainMenuCR()
	{
		StartCoroutine(GoToMainMenu());
	}
	
	public IEnumerator GoToMainMenu() {
		GoToLevelSelect();
		yield return true;
//		Transform tgtT = transform.parent.Find("HUD Controller").Find("Main Menu GUI");
//		
//		currentLevelId = 0;
//		transform.parent.GetComponent<PauseMenu>().SetActive(false);		
//		if (Application.loadedLevelName != "Wait") {
//			if (!onSplash)
//			{
//				GameObject.Find("HUD Controller").GetComponent<SceneFading>().FadeIn(1.0f);
//				yield return new WaitForSeconds(1.0f);
//			}
//			Application.LoadLevel("Wait");
//		}
//		bonusMode = false;
//		currentBonusId = 0;
//		transform.parent.Find("HUD Controller").Find("Select Level Menu GUI").gameObject.SetActive(false);
//		transform.parent.FindChild("HUD Controller").GetComponent<GUIController>().ResetButtonState(tgtT);
//		tgtT.gameObject.SetActive(true);
//		yield return true;
	}
	
	public void GoToSplash() {
		currentLevelId = 0;
		currentBonusId = 0;
		bonusMode = false;
		Application.LoadLevel("Splash");
		transform.parent.GetComponent<PauseMenu>().SetActive(false);
	}
}
