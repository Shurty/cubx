﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	private bool level = false;
	public Material activeMaterial;
	public Material inactiveMaterial;

	// Use this for initialization
	void Start () {
		this.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material = activeMaterial;
		this.transform.GetChild(1).GetChild(1).GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material = inactiveMaterial;
	}
	
	// Update is called once per frame
	void Update () {
		if (Paused.getPaused())
			return;
		if (Input.GetButtonDown("Vertical"))
		{
			level = !level;
			if (level)
			{
				this.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material = inactiveMaterial;
				this.transform.GetChild(1).GetChild(1).GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material = activeMaterial;
			}
			else
			{
				this.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material = activeMaterial;
				this.transform.GetChild(1).GetChild(1).GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material = inactiveMaterial;
			}
		}

		if (Input.GetButton("Jump"))
		{
			if (!level)
				GameObject.Find("Scene Controller").GetComponent<SceneSwitch>().GoToLevelSelect();
			else
				Application.Quit();	
		}
	}
}
