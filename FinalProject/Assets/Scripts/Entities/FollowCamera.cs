﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {
	
	public GameObject cameraTarget;
	public Vector3 distancePosition;
	public Vector3 rotationAngle = new Vector3(15.0f, 0.0f, 0.0f);
	public bool shaking = false;
	public bool followTarget = true;
	public float shakeDelay = 1;
	
	private Vector3 shakeTargetPos;
	private float nextShake = 0;

	// Use this for initialization
	void Start () {
		nextShake = Time.timeSinceLevelLoad;
	}
	
	// Update is called once per frame
	void Update () {
		if (!cameraTarget)
			return ;
		if (Time.timeSinceLevelLoad > nextShake) {
			nextShake = Time.timeSinceLevelLoad + shakeDelay;
			shakeTargetPos = new Vector3(Random.Range(-0.5f, 0.5f), 0.0f, Random.Range(-0.5f, 0.5f));
		}
		//new Vector3(0.9268043, 1.416875, -9.0)
		Vector3 pos = transform.position;
		Vector3 rot = transform.localEulerAngles;
		Vector3 targetPos;
		
		if (followTarget)
			targetPos = cameraTarget.transform.position - distancePosition;
		else
			targetPos = pos;
		
		if (rot.z >= 359) { rot.z -= 360; }
		
		if (shaking)
			transform.position = Vector3.Slerp(pos, targetPos + shakeTargetPos, 1f);
		else
			transform.position = Vector3.Lerp(pos, targetPos, 1f);
		
		transform.localEulerAngles = Vector3.Lerp(rot, rotationAngle, 0.05f);
		if (this.gameObject.transform.position.y < 3.48f){
			this.gameObject.transform.position = new Vector3(this.transform.position.x, 3.48f, this.transform.position.z);
		}
	}
}
