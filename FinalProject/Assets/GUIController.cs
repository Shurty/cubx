﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
			
	public void ResetButtonState(Transform contentElement) {
		int elemCount = contentElement.childCount;	
		while (elemCount > 0)
		{
			Transform ent = contentElement.GetChild(elemCount - 1);
			if (ent.Find("Click Plane")) {
				ent.Find("Click Plane").GetComponent<GUIButton>().SetHovered(false);
			}
			elemCount--;	
			if (ent.transform.childCount > 0)
				ResetButtonState(ent.transform);
		}		
	}
	
}
