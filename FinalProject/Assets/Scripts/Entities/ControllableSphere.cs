﻿using UnityEngine;
using System.Collections;

public class ControllableSphere : MonoBehaviour {
	
	private Transform triggerColision; // The current trigger element the sphere is in collision with (activators)
	
	// Use this for initialization
	public void Start () {
		triggerColision = null;
	}
	
	public void SetTriggerColision(Transform oth)
	{
		triggerColision = oth;	
	}
	
	// Update is called once per frame
	public void Update () {

	}
	
	public IEnumerator Remove()
	{
		tag = "Destroying";
	
		if (triggerColision)
			triggerColision.SendMessage("OnColliderDeletion", this.transform);
		yield return StartCoroutine(FadeOut());
		if (triggerColision)
			triggerColision.SendMessage("OnColliderDeletion", this.transform);
		Destroy(this.gameObject);
	}
	
	public IEnumerator FadeIn()
	{
		this.transform.FindChild("Creation Particle Emitter").GetComponent<ParticleEmitter>().emit = true;
		for (float tm = 0.0f; tm < 1.0f; tm += Time.deltaTime * 2.2f)
		{
			this.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, tm);
			yield return true;
		}
		this.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		this.transform.FindChild("Creation Particle Emitter").GetComponent<ParticleEmitter>().emit = false;
		yield return true;
	}
	
	public IEnumerator FadeOut()
	{
		this.transform.FindChild("Creation Particle Emitter").GetComponent<ParticleEmitter>().emit = true;
		for (float tm = 1.0f; tm > 0.0f; tm -= Time.deltaTime * 2.2f)
		{
			this.GetComponent<MeshRenderer>().material.color = new Color(tm, tm, tm, tm);
			yield return true;
		}
		this.GetComponent<MeshRenderer>().material.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
		this.transform.FindChild("Creation Particle Emitter").GetComponent<ParticleEmitter>().emit = false;
		yield return true;
	}
	
	public IEnumerator InitializeSphere(GameObject spawner)
	{
		GameObject camera = GameObject.Find("Main Camera");
		
		spawner.transform.FindChild("Particle System").GetComponent<ParticleSystem>().Play();
		camera.GetComponent<FollowCamera>().cameraTarget = this.gameObject;
		camera.GetComponent<FollowCamera>().rotationAngle = new Vector3(15, 0, 0);
		this.tag = "Ghost";
		this.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
		this.GetComponent<Rigidbody>().isKinematic = false;
		this.GetComponent<SphereCollider>().enabled = false;
		this.GetComponent<Rigidbody>().AddForce(new Vector3(0, 350, 0));
		yield return new WaitForSeconds(0.5f);
		spawner.transform.FindChild("Particle System").GetComponent<ParticleSystem>().Stop();
		this.GetComponent<SphereCollider>().enabled = true;
		yield return StartCoroutine(FadeIn());
		this.GetComponent<PlayerControll>().activate();
		this.tag = "Player";
		
		
	}
}
